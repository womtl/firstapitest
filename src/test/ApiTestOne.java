import org.junit.Assert;
import org.junit.Ignore;
import org.junit.Test;
import org.junit.jupiter.api.DisplayName;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URI;
import java.net.URISyntaxException;
import java.net.URL;
import java.net.http.HttpClient;
import java.net.http.HttpRequest;
import java.net.http.HttpResponse;
import java.nio.charset.StandardCharsets;

import static org.junit.jupiter.api.Assertions.assertEquals;


public class ApiTestOne {

    @Test
    @DisplayName("PUT/updateUser. Обновление пользователя")
    public void PutUpdateUser() throws IOException, URISyntaxException, InterruptedException {
        String s ="{\"name\":\"geo\",\"job\":\"qa\"}";
        byte[] out = s.getBytes(StandardCharsets.UTF_8);

        HttpRequest request = HttpRequest.newBuilder()
                .PUT(HttpRequest.BodyPublishers.ofByteArray(out))
                .uri(new URI("https://reqres.in/api/users/1"))
                .build();

        HttpResponse response = HttpClient.newHttpClient().send(request, HttpResponse.BodyHandlers.ofString());
        System.out.println(response.body());
        System.out.println(response.statusCode());
        assertEquals(200, response.statusCode());
    }

    @Test
    @DisplayName("PUT/updateUser. Правильное обновление пользователя")
    public void successPutUpdateUser() throws IOException, InterruptedException {
        HttpRequest requestPut = HttpRequest.newBuilder()
                .header("Content-Type", "application/json")
                .PUT(HttpRequest.BodyPublishers.ofString("{\"name\": \"geo\",\"job\": \"qa\"}"))
                .uri(URI.create("https://reqres.in/api/users/2"))
                .build();
        HttpResponse responsePut = HttpClient.newHttpClient().send(requestPut, HttpResponse.BodyHandlers.ofString());

        System.out.println(responsePut.body());
        System.out.println(responsePut.statusCode());

        assertEquals(200, responsePut.statusCode());
    }

    @Test
    @DisplayName("Вызов метода DELETE /deleteUser. Удаление пользователя")
    public void deleteUser() throws IOException, InterruptedException {
        HttpRequest requestPut = HttpRequest.newBuilder()
                .header("Content-Type", "application/json")
                .DELETE()
                .uri(URI.create("https://reqres.in/api/users/2"))
                .build();
        HttpResponse responseDelete = HttpClient.newHttpClient().send(requestPut, HttpResponse.BodyHandlers.ofString());

        System.out.println(responseDelete.body());
        System.out.println(responseDelete.statusCode());

        assertEquals(203, responseDelete.statusCode());
    }


    @Ignore("Тест Маргариты")
    @Test
    @DisplayName("Вызов метода POST /createUser. Создание нового пользователя")
    public void ThirdTest() throws IOException {
        StringBuilder result = new StringBuilder();
        URL url = new URL("https://reqres.in/api/users/2");
        HttpURLConnection conn = (HttpURLConnection) url.openConnection();
        conn.setRequestMethod("GET");
        try (BufferedReader reader = new BufferedReader(
                new InputStreamReader(conn.getInputStream()))) {
            for (String line; (line = reader.readLine()) != null; ) {
                result.append(line);
            }
        }
        System.out.println(result);
    }
}
